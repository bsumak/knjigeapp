#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using knjigeapp.Models;

namespace knjigeapp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KnjigaController : ControllerBase
    {
        private IKnjigaRepository rep;

        public KnjigaController([FromServices] IKnjigaRepository rep)
        {
            this.rep = rep;
        }

        // GET: api/<KnjigaController>
        [HttpGet]
        public IEnumerable<Knjiga> Get()
        {
            return rep.GetAll();
        }

        // GET api/<KnjigaController>/5
        [HttpGet("{kljuc}", Name = "GetKnjiga")]
        public IActionResult Get(string kljuc)
        {
            Knjiga najdi = rep.Find(kljuc);
            if (najdi==null)
            {
                return NotFound($"Knjige s kljuc={kljuc} ne najdem");
            }
            else return new ObjectResult(najdi);
        }

        // POST api/<KnjigaController>
        [HttpPost]
        public IActionResult Post([FromBody] Knjiga knjiga)
        {
            bool dodano = rep.Add(knjiga);
            if (dodano)
            {
                return CreatedAtRoute("GetKnjiga", new { kljuc = knjiga.Kljuc }, knjiga);
            }
            else
                return BadRequest("Knjige ni bilo možno dodati");
        }

        // PUT api/<KnjigaController>/5
        [HttpPut("{kljuc}")]
        public IActionResult Put(string kljuc, [FromBody] Knjiga knjiga)
        {
            if (kljuc == null || kljuc != knjiga.Kljuc)
            {
                return BadRequest($"{kljuc} != {knjiga.Kljuc}");
            } else {
                Knjiga obstojeca = rep.Find(kljuc);
                if (obstojeca == null)
                {
                    return NotFound($"Knjiga s kljuc={kljuc} ne obstaja!");
                } else
                {
                    bool posodobljeno = rep.Update(kljuc, knjiga);
                    return NoContent();                    
                }                
            }            
        }

        // DELETE api/<KnjigaController>/5
        [HttpDelete("{kljuc}")]
        public IActionResult Delete(string kljuc)
        {
            Knjiga obstojeca = rep.Find(kljuc);
            if (obstojeca == null)
            {
                return NotFound($"Knjiga s kljuc={kljuc} ne obstaja!");
            }
            else
            {
                rep.Delete(kljuc);
                return NoContent();
            }
        }
    }
}
