﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace knjigeapp.Models
{
    public class Knjiga
    {
        public string Kljuc { get; set; }
        public string Avtor { get; set; }
        public string Naslov { get; set; }
        public string Isbn { get; set; }
        public int LetoIzida { get; set; }
    }
}
