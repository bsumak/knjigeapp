﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace knjigeapp.Models
{
    public interface IKnjigaRepository
    {

        IEnumerable<Knjiga> GetAll();
        Knjiga Find(string kljuc);
        bool Add(Knjiga knjiga);
        bool Update(string kljuc, Knjiga knjiga);
        bool Delete(string kljuc);

    }
}
