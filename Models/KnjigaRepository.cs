﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace knjigeapp.Models
{
    public class KnjigaRepository : IKnjigaRepository
    {
        private static ConcurrentDictionary<string, Knjiga> _knjige = new ConcurrentDictionary<string, Knjiga>();

        public KnjigaRepository()
        {
            Add(new Knjiga { Avtor = "Thomas Erl", Naslov = "SOA patterns", Isbn = "1191912-2211", LetoIzida = 2019 });
            Add(new Knjiga { Avtor = "Martin Fowler", Naslov = "Microservices", Isbn = "3243-5434", LetoIzida = 2020 });
        }

        public IEnumerable<Knjiga> GetAll()
        {
            return _knjige.Values;
        }

        public Knjiga Find(string kljuc)
        {
            Knjiga vrni;
            _knjige.TryGetValue(kljuc, out vrni);
            return vrni;
            
        }

        public bool Add(Knjiga knjiga)
        {
            knjiga.Kljuc = Guid.NewGuid().ToString();
            return _knjige.TryAdd(knjiga.Kljuc, knjiga);
        }


        public bool Update(string kljuc, Knjiga knjiga)
        {
            Knjiga obstojeca = Find(kljuc);
            return _knjige.TryUpdate(kljuc, knjiga, obstojeca);
        }

        public bool Delete(string kljuc)
        {
            Knjiga brisi = Find(kljuc);
            return _knjige.TryRemove(kljuc, out brisi);
        }

        

        
    }
}
